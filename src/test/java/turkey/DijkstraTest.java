package turkey;

import org.junit.Test;

import java.util.List;

public class DijkstraTest {

    Dijkstra dk = new Dijkstra();

    @Test
    public void test(){
        long startTime =  System.currentTimeMillis();
        // 第一步：假设的比赛场地，其中设置值为10的表示障碍物
        int[][] localMap = new int[10][10];
        localMap[4][4] = 100;
        localMap[4][5] = 100;
        localMap[4][6] = 100;
//        int[][] data = {
//                {1, 1, 1, 1},
//                {1, 1, 10, 1},
//                {1, 10, 1, 1}
//        };
        // 第二步：根据比赛场地计算权重矩阵
        int[][] WeightMatrix = dk.creatWeightMatrix(localMap);
        // 第三步：给出 起点和终点计算出最短路径
        int start = 35;
        int end = 65;
        List<String> path = dk.dijkstra(WeightMatrix, start, end);

        System.out.print("从" + start + "出发到" + end + "的行走路径为：");
        for (int i = 0; i < path.size(); i++) {
            System.out.print(path.get(i) + "---->");
        }
        System.out.println();
        long endTime =  System.currentTimeMillis();
        long usedTime = endTime-startTime;
        System.out.println(endTime + "-" + startTime + "="  + usedTime);
    }
}