package turkey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.type.MapType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
//@JsonIgnoreProperties(value = {"targetWith"})
public class PlayerMap {

    // 选手阵营，阵营分cao和shu两派，根据自己分配到的阵营查看地图
    private String name;
    // 所选地图有多少列
    private int colLen = 0;
    // 所选地图有多少行
    private int rowLen = 0;
    // 敌方当前得分数
    private int opponentTargetNum = 0;
    // 我方当前得分数
    private int selfTargetNum = 0;
    // 敌方当前已夺取宝物后移动步数（夺取宝物超过一定步数将得分）
    private int opponentTargetStep = 0;
    // 我方当前已夺取宝物后移动步数
    private int selfTargetStep = 0;
    // 宝物当前在谁身上
    private String targetWith;
    // 我方角色复活点列
    private Integer selfGraveColLen = 0;
    // 我方角色复活点行
    private Integer selfGraveRowLen = 0;
    // 地图情况
    private List<List<PosInfo>> map;
}
