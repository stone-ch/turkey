package turkey;

import lombok.Data;

@Data
public class ClientRes {
    private Direct player1;
    private Direct player2;
    private Direct player3;
    private Direct player4;
}
