package turkey;

public enum Direct {
    UP(0,"上移"),
    DOWN(1,"下移"),
    RIGHT(2,"右移"),
    LEFT(3,"左移"),
    STAY(4,"不移");

    private String message;
    private int code;

    Direct(int code, String message) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public int getCode() {
        return this.code;
    }
}
