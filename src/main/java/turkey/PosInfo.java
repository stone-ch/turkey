package turkey;

import lombok.Data;

@Data
public class PosInfo {
    // 因为有战争迷雾效果，所以当角色未打开视野时此值为true
    private boolean unknown = false;
    // 为true时表示此道路为障碍物，不得移动到此坐标上
    private boolean block = false;
    // 为true时表示此可移动道路，可以将角色移动到此坐标上
    private boolean road = false;
    // 角色分为cao和shu两派阵营。每派可以控制4个角色，分别对应1234
    private boolean cao1 = false;
    private boolean cao2 = false;
    private boolean cao3 = false;
    private boolean cao4 = false;
    private boolean shu1 = false;
    private boolean shu2 = false;
    private boolean shu3 = false;
    private boolean shu4 = false;
    // 该坐标上如果有宝物，则此值为true
    private boolean target = false;
}
